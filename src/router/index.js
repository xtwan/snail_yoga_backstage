import Vue from 'vue'
import Router from 'vue-router'
import login from '@/components/login/login' //登录
import route from '@/components/route' //首页路由组件
import Library_management from '@/pages/homePage/Library_management' //馆主管理
import Venue_management from '@/pages/homePage/Venue_management' //场馆管理
import member from '@/pages/homePage/member' //会员管理
import course_management from '@/pages/homePage/course_management' //课程管理
import stencil_plate from '@/pages/homePage/stencil_plate' //管理课程模板
import BookingManagement from '@/pages/homePage/BookingManagement' //预约管理
import BossIncome from '@/pages/homePage/BossIncome' //Boss收益



Vue.use(Router)

export default new Router({
    routes: [
        //   首页

        {
            path: '/',
            // component: login
            redirect: 'Library_management'
        },
        {
            path: '/login',
            // component: login
            component: resolve => require(['@/components/login/login'], resolve)
        },

        // 地图
        {
            path: '/LocationSearchdw',
            // component: Library_management
            component: resolve => require(['@/pages/homePage/LocationSearchdw'], resolve),

        },
        // 首页超管路由
        {
            path: '/route',
            // component: login
            component: resolve => require(['@/components/route'], resolve),
            children: [{
                    path: '/Library_management',
                    // component: Library_management
                    component: resolve => require(['@/pages/homePage/Library_management'], resolve),

                },
                {
                    path: '/Venue_management',
                    // component: Library_management
                    component: resolve => require(['@/pages/homePage/Venue_management'], resolve),

                },
                {
                    path: '/member',
                    // component: Library_management
                    component: resolve => require(['@/pages/homePage/member'], resolve),

                },
                {
                    path: '/course_management',
                    // component: Library_management
                    component: resolve => require(['@/pages/homePage/course_management'], resolve),

                },
                {
                    path: '/stencil_plate',
                    // component: Library_management
                    component: resolve => require(['@/pages/homePage/stencil_plate'], resolve),

                },
                {
                    path: '/BookingManagement',
                    // component: Library_management
                    component: resolve => require(['@/pages/homePage/BookingManagement'], resolve),

                },
                {
                    path: '/BossIncome',
                    // component: Library_management
                    component: resolve => require(['@/pages/homePage/BossIncome'], resolve),

                },



                {
                    path: '/',
                    // component: login
                    redirect: 'Library_management'
                },
            ]
        },
        // 首页馆主路由
        {
            path: '/Library_routing',
            // component: Library_routing
            component: resolve => require(['@/components/Library_routing'], resolve),
            children: [{

                    path: '/Boss_information',
                    // component: Library_routing
                    component: resolve => require(['@/pages/Library_owner/Boss_information'], resolve),
                },
                // 管理前台服务
                {

                    path: '/service',
                    // component: Library_routing
                    component: resolve => require(['@/pages/Library_owner/service'], resolve),
                },
                // 场馆信息
                {

                    path: '/Venue_information',
                    // component: Library_routing
                    component: resolve => require(['@/pages/Library_owner/Venue_information'], resolve),
                },
                // 场馆会员管理
                {

                    path: '/membership',
                    // component: Library_routing
                    component: resolve => require(['@/pages/Library_owner/membership'], resolve),
                },
                // 场馆管理
                {

                    path: '/venue_course',
                    // component: Library_routing
                    component: resolve => require(['@/pages/Library_owner/venue_course'], resolve),
                },
                // 场馆课程模版
                {

                    path: '/masterPlate',
                    // component: Library_routing
                    component: resolve => require(['@/pages/Library_owner/masterPlate'], resolve),
                },
                //    预约管理
                {

                    path: '/VenueSite',
                    // component: Library_routing
                    component: resolve => require(['@/pages/Library_owner/VenueSite'], resolve),
                },
                // 我的收益
                {

                    path: '/MyIncome',
                    // component: Library_routing
                    component: resolve => require(['@/pages/Library_owner/MyIncome'], resolve),
                },


                {
                    path: '/',
                    // component: login
                    redirect: 'Boss_information'
                },


            ]
        },

        // 前台管理路由
        {
            path: '/stage_routing',
            // component: Library_routing
            component: resolve => require(['@/components/stage_routing'], resolve),
            children: [{

                    path: '/fontline_management',
                    // component: Library_routing
                    component: resolve => require(['@/pages/stage/fontline_management'], resolve),
                },
                {

                    path: '/desk_member',
                    // component: desk_member
                    component: resolve => require(['@/pages/stage/desk_member'], resolve),
                },
                {

                    path: '/desk_course',
                    // component: desk_member
                    component: resolve => require(['@/pages/stage/desk_course'], resolve),
                },
                {

                    path: '/desk_reservation',
                    // component: desk_member
                    component: resolve => require(['@/pages/stage/desk_reservation'], resolve),
                },




                {
                    path: '/',
                    // component: login
                    redirect: 'fontline_management'
                },



            ],

        }





    ]

})