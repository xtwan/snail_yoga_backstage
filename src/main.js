// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import ElementUI from 'element-ui' //手动变红
import '../node_modules/element-ui/lib/theme-chalk/index.css' //手动变红
import qs from 'qs';
import axios from "axios";
import N3Components from 'N3-components'
import 'N3-components/dist/index.min.css'
// import iView from 'iview';
// import 'iview/dist/styles/iview.css';    // 使用 CSS

import iView from 'iview'; // 导入组件库
import 'iview/dist/styles/iview.css'; // 导入样式

// Vue.use(iView);
Vue.use(iView)

Vue.use(N3Components)

Vue.use(N3Components, 'en')

setInterval(()=>{
},)


Vue.prototype.$axios = axios
Vue.prototype.$qs = qs;
axios.defaults.baseURL = 'http://yogahome.top';
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
Vue.config.productionTip = false
axios.defaults.withCredentials=true;
axios.interceptors.response.use(function (response) {
    // 对响应数据做点什么
    Vue.prototype.$state = response.data.code
    return response;
  }, function (error) {
    // 对响应错误做点什么
    return Promise.reject(error);
  });
// axios.post('http://yogahome.shop/yujiaextend/gymadmin/checkIsHaveNewBooking',)
// .then(function (response) {
//   window.sessionStorage.setItem('ckrs',response.data.data[0])
// })
// .catch(function (error) {
//   console.log(error);
// });
// setInterval(()=>{
//     axios.post('http://yogahome.shop/yujiaextend/gymadmin/checkIsHaveNewBooking',)
//   .then(function (response) {
//     var ckrs=window.sessionStorage.getItem('ckrs')
//     if (response.data.data[0] > ckrs) {
//       var hua=response.data.data[1]
//       var huas=hua.action_date+'日'+hua.attend+'人约了'+hua.action_name
//       Vue.prototype.$states = huas
//       alert(1)
//     }


//   })
// },1000)
Vue.use(ElementUI) //手动变红

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    components: { App },
    template: '<App/>'
})